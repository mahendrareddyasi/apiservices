﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ODataAPIServices.Models
{
    public partial class EasyTrackContext : DbContext
    {
        //public EasyTrackContext()
        //{
        //}

        public EasyTrackContext(DbContextOptions<EasyTrackContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Clients> Clients { get; set; }
        public virtual DbSet<Containers> Containers { get; set; }
        public virtual DbSet<Drivers> Drivers { get; set; }
        public virtual DbSet<FlightMode> FlightMode { get; set; }
        public virtual DbSet<TripDetails> TripDetails { get; set; }
        public virtual DbSet<UserRoles> UserRoles { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Vehicles> Vehicles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost;Database=EasyTrack;User Id=sa;Password=sa123;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Clients>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.ClientPhoneNo).HasMaxLength(300);

                entity.Property(e => e.InsertedDate).HasColumnType("datetime");

                entity.Property(e => e.InsertedUser)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedUser)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Containers>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContainerDescription)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.ContainerNo).HasMaxLength(300);

                entity.Property(e => e.InsertedDate).HasColumnType("datetime");

                entity.Property(e => e.InsertedUser)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedUser)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Drivers>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.BankAccountNo).HasMaxLength(50);

                entity.Property(e => e.BankName).HasMaxLength(50);

                entity.Property(e => e.ContactNo).HasMaxLength(50);

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.DateOfJoining).HasColumnType("date");

                entity.Property(e => e.DriverMissionStatusCode).HasMaxLength(50);

                entity.Property(e => e.DrivingLicenceExpiryDate).HasColumnType("date");

                entity.Property(e => e.DrivingLicenceNo).HasMaxLength(50);

                entity.Property(e => e.EmployeeNo).HasMaxLength(50);

                entity.Property(e => e.FirstName).HasMaxLength(150);

                entity.Property(e => e.InsertedDate).HasColumnType("datetime");

                entity.Property(e => e.InsertedUser)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(150);

                entity.Property(e => e.MiddleName).HasMaxLength(150);

                entity.Property(e => e.Photo).HasColumnType("image");

                entity.Property(e => e.Remark1).HasMaxLength(255);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedUser)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.VehicleAssigned).HasMaxLength(50);
            });

            modelBuilder.Entity<FlightMode>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TripDetails>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DeliveryPicture).HasColumnType("image");

                entity.Property(e => e.DriverAssigned)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.InsertedDate).HasColumnType("datetime");

                entity.Property(e => e.InsertedUser).HasMaxLength(50);

                entity.Property(e => e.Status).IsRequired();

                entity.Property(e => e.TripEnded).HasColumnType("datetime");

                entity.Property(e => e.TripPayment).HasColumnType("money");

                entity.Property(e => e.TripStarted).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedUser).HasMaxLength(50);

                entity.Property(e => e.VehicleNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.ClientNavigation)
                    .WithMany(p => p.TripDetails)
                    .HasForeignKey(d => d.Client)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TripDetai__Clien__412EB0B6");

                entity.HasOne(d => d.ContainerNumberNavigation)
                    .WithMany(p => p.TripDetails)
                    .HasForeignKey(d => d.ContainerNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TripDetai__Conta__4222D4EF");

                entity.HasOne(d => d.DriverAssignedNavigation)
                    .WithMany(p => p.TripDetails)
                    .HasForeignKey(d => d.DriverAssigned)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TripDetai__Drive__403A8C7D");

                entity.HasOne(d => d.VehicleNumberNavigation)
                    .WithMany(p => p.TripDetails)
                    .HasForeignKey(d => d.VehicleNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TripDetai__Vehic__3F466844");
            });

            modelBuilder.Entity<UserRoles>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK__Users__1788CCAC8A1A243E");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.EmailId)
                    .IsRequired()
                    .HasColumnName("EmailID")
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password).IsRequired();

                entity.HasOne(d => d.UserRoleNavigation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.UserRole)
                    .HasConstraintName("FK__Users__UserRole__49C3F6B7");
            });

            modelBuilder.Entity<Vehicles>(entity =>
            {
                entity.HasKey(e => e.VehicleNumber)
                    .HasName("PK_tmpVehicles");

                entity.Property(e => e.VehicleNumber)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.BodyType).HasMaxLength(255);

                entity.Property(e => e.Color).HasMaxLength(255);

                entity.Property(e => e.CompanyCode).HasMaxLength(255);

                entity.Property(e => e.DoorNo).HasMaxLength(50);

                entity.Property(e => e.DriverAssigned).HasMaxLength(50);

                entity.Property(e => e.InsertedDate).HasColumnType("datetime");

                entity.Property(e => e.InsertedUser).HasMaxLength(50);

                entity.Property(e => e.InsuranceCompany).HasMaxLength(255);

                entity.Property(e => e.InsuranceExpiryDate).HasColumnType("date");

                entity.Property(e => e.LicensePlateNumber).HasMaxLength(50);

                entity.Property(e => e.Make).HasMaxLength(255);

                entity.Property(e => e.Model).HasMaxLength(50);

                entity.Property(e => e.RegistrationExpiryDate).HasColumnType("date");

                entity.Property(e => e.Remark1).HasMaxLength(255);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedUser).HasMaxLength(50);

                entity.Property(e => e.Vin)
                    .HasColumnName("VIN")
                    .HasMaxLength(255);

                entity.Property(e => e.Year).HasMaxLength(50);
            });
        }
    }
}
