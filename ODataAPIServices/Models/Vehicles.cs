﻿using System;
using System.Collections.Generic;

namespace ODataAPIServices.Models
{
    public partial class Vehicles
    {
        public Vehicles()
        {
            TripDetails = new HashSet<TripDetails>();
        }

        public string VehicleNumber { get; set; }
        public string DoorNo { get; set; }
        public string LicensePlateNumber { get; set; }
        public string Vin { get; set; }
        public double? MasterListNo { get; set; }
        public string CompanyCode { get; set; }
        public string BodyType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public string Color { get; set; }
        public string InsuranceCompany { get; set; }
        public DateTime? InsuranceExpiryDate { get; set; }
        public DateTime? RegistrationExpiryDate { get; set; }
        public long? KmIn { get; set; }
        public long? KmOut { get; set; }
        public string Remark1 { get; set; }
        public string DriverAssigned { get; set; }
        public string InsertedUser { get; set; }
        public DateTime? InsertedDate { get; set; }
        public string UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long DataVersion { get; set; }

        public virtual ICollection<TripDetails> TripDetails { get; set; }
    }
}
