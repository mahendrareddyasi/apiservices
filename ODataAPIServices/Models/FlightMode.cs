﻿using System;
using System.Collections.Generic;

namespace ODataAPIServices.Models
{
    public partial class FlightMode
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
