﻿using System;
using System.Collections.Generic;

namespace ODataAPIServices.Models
{
    public partial class Drivers
    {
        public Drivers()
        {
            TripDetails = new HashSet<TripDetails>();
        }

        public string Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PermanentAddess { get; set; }
        public byte[] Photo { get; set; }
        public string EmployeeNo { get; set; }
        public string ContactNo { get; set; }
        public bool? PayByCash { get; set; }
        public string BankName { get; set; }
        public string BankAccountNo { get; set; }
        public DateTime? DateOfJoining { get; set; }
        public string DrivingLicenceNo { get; set; }
        public DateTime? DrivingLicenceExpiryDate { get; set; }
        public string Remark1 { get; set; }
        public string VehicleAssigned { get; set; }
        public string DriverMissionStatusCode { get; set; }
        public string InsertedUser { get; set; }
        public DateTime InsertedDate { get; set; }
        public string UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long DataVersion { get; set; }

        public virtual ICollection<TripDetails> TripDetails { get; set; }
    }
}
