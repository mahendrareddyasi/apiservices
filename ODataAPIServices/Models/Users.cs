﻿using System;
using System.Collections.Generic;

namespace ODataAPIServices.Models
{
    public partial class Users
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Password { get; set; }
        public bool IsEmailVerified { get; set; }
        public Guid ActivationCode { get; set; }
        public string ConfirmPassword { get; set; }
        public int? UserRole { get; set; }

        public virtual UserRoles UserRoleNavigation { get; set; }
    }
}
