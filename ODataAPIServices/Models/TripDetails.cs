﻿using System;
using System.Collections.Generic;

namespace ODataAPIServices.Models
{
    public partial class TripDetails
    {
        public int Id { get; set; }
        public string VehicleNumber { get; set; }
        public string DriverAssigned { get; set; }
        public int Client { get; set; }
        public string Status { get; set; }
        public DateTime? TripStarted { get; set; }
        public DateTime? TripEnded { get; set; }
        public decimal? TripPayment { get; set; }
        public byte[] DeliveryPicture { get; set; }
        public int ContainerNumber { get; set; }
        public string InsertedUser { get; set; }
        public DateTime? InsertedDate { get; set; }
        public string UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long DataVersion { get; set; }

        public virtual Clients ClientNavigation { get; set; }
        public virtual Containers ContainerNumberNavigation { get; set; }
        public virtual Drivers DriverAssignedNavigation { get; set; }
        public virtual Vehicles VehicleNumberNavigation { get; set; }
    }
}
