﻿using System;
using System.Collections.Generic;

namespace ODataAPIServices.Models
{
    public partial class Clients
    {
        public Clients()
        {
            TripDetails = new HashSet<TripDetails>();
        }

        public int Id { get; set; }
        public string ClientName { get; set; }
        public string ClientPhoneNo { get; set; }
        public string InsertedUser { get; set; }
        public DateTime InsertedDate { get; set; }
        public string UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }

        public virtual ICollection<TripDetails> TripDetails { get; set; }
    }
}
