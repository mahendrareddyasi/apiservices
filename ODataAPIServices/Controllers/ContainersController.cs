﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ODataAPIServices.Models;

namespace ODataAPIServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContainersController : ControllerBase
    {
        private readonly EasyTrackContext _context;

        public ContainersController(EasyTrackContext context)
        {
            _context = context;
        }

        // GET: api/Containers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Containers>>> GetContainers()
        {
            return await _context.Containers.ToListAsync();
        }

        // GET: api/Containers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Containers>> GetContainers(int id)
        {
            var containers = await _context.Containers.FindAsync(id);

            if (containers == null)
            {
                return NotFound();
            }

            return containers;
        }

        // PUT: api/Containers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutContainers(int id, Containers containers)
        {
            if (id != containers.Id)
            {
                return BadRequest();
            }

            _context.Entry(containers).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContainersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Containers
        [HttpPost]
        public async Task<ActionResult<Containers>> PostContainers(Containers containers)
        {
            _context.Containers.Add(containers);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetContainers", new { id = containers.Id }, containers);
        }

        // DELETE: api/Containers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Containers>> DeleteContainers(int id)
        {
            var containers = await _context.Containers.FindAsync(id);
            if (containers == null)
            {
                return NotFound();
            }

            _context.Containers.Remove(containers);
            await _context.SaveChangesAsync();

            return containers;
        }

        private bool ContainersExists(int id)
        {
            return _context.Containers.Any(e => e.Id == id);
        }
    }
}
