﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ODataAPIServices.Models;

namespace ODataAPIServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightModesController : ControllerBase
    {
        private readonly EasyTrackContext _context;

        public FlightModesController(EasyTrackContext context)
        {
            _context = context;
        }

        // GET: api/FlightModes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FlightMode>>> GetFlightMode()
        {
            return await _context.FlightMode.ToListAsync();
        }

        // GET: api/FlightModes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FlightMode>> GetFlightMode(int id)
        {
            var flightMode = await _context.FlightMode.FindAsync(id);

            if (flightMode == null)
            {
                return NotFound();
            }

            return flightMode;
        }

        // PUT: api/FlightModes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFlightMode(int id, FlightMode flightMode)
        {
            if (id != flightMode.Id)
            {
                return BadRequest();
            }

            _context.Entry(flightMode).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FlightModeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FlightModes
        [HttpPost]
        public async Task<ActionResult<FlightMode>> PostFlightMode(FlightMode flightMode)
        {
            _context.FlightMode.Add(flightMode);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFlightMode", new { id = flightMode.Id }, flightMode);
        }

        // DELETE: api/FlightModes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FlightMode>> DeleteFlightMode(int id)
        {
            var flightMode = await _context.FlightMode.FindAsync(id);
            if (flightMode == null)
            {
                return NotFound();
            }

            _context.FlightMode.Remove(flightMode);
            await _context.SaveChangesAsync();

            return flightMode;
        }

        private bool FlightModeExists(int id)
        {
            return _context.FlightMode.Any(e => e.Id == id);
        }
    }
}
